This repository contains python and data files used to generate data used by the following visualization on Observable: 

https://observablehq.com/@profgannod/computer-science-student-journey

The primary python script is *sankey.py*

Usage:
`python3 sankey.py *majorname* *outputfile*.json`

The python file depends on the dataset provided in 2017Cohortcsv.csv
