#!/bin/sh
python3 sankey.py "Agriculture" AGRI.json
python3 sankey.py "Basic Business" BBUS.json
python3 sankey.py "Basic Engineering" BE.json
python3 sankey.py "Biology" BIOL.json
python3 sankey.py "Chemical Engineering" CHE.json
python3 sankey.py "Chemistry" CHEM.json
python3 sankey.py "Civil Engineering" CE.json
python3 sankey.py "Communication" COM.json
python3 sankey.py "Computer Engineering" CMPE.json
python3 sankey.py "Computer Science" CSC.json
python3 sankey.py "Early Childhood Education" ECED.json
python3 sankey.py "Electrical Engineering" EE.json
python3 sankey.py "Elementary Education" ELEM.json
python3 sankey.py "Engineering" ENGR.json
python3 sankey.py "Engineering Technology" ET.json
python3 sankey.py "English" ENG.json
python3 sankey.py "Environmntl  Sustain Studies" ESS.json
python3 sankey.py "Exercise Sci PhysEd Wellness" EXPW.json
python3 sankey.py "Fine Arts" ART.json
python3 sankey.py "Foreign Languages" FL.json
python3 sankey.py "General Curriculum" GECU.json
python3 sankey.py "General Health Studies" GHS.json
python3 sankey.py "Geosciences" GEOS.json
python3 sankey.py "History - B.A." HIBA.json
python3 sankey.py "History - B.S." HIBS.json
python3 sankey.py "Human Ecology" HEC.json
python3 sankey.py "Interdisciplinary Studies" LIST.json
python3 sankey.py "Mathematics" MATH.json
python3 sankey.py "Mechanical Engineering" ME.json
python3 sankey.py "Multidisciplinary Studies" MDS.json
python3 sankey.py "Music" MUS.json
python3 sankey.py "Nursing - LD" NULO.json
python3 sankey.py "Physics" PHYS.json
python3 sankey.py "Political Science" POLS.json
python3 sankey.py "Pre-Dental Hygiene" PDHY.json
python3 sankey.py "Pre-Dentistry" PDEN.json
python3 sankey.py "Pre-Law" GEPL.json
python3 sankey.py "Pre-Medicine" PMED.json
python3 sankey.py "Pre-Occupational Therapy" POTH.json
python3 sankey.py "Pre-Optometry" POPT.json
python3 sankey.py "Pre-Pharmacy" PPHA.json
python3 sankey.py "Pre-Physical Therapy" PPTH.json
python3 sankey.py "Pre-Physicians Assistant" PPA.json
python3 sankey.py "Psychology" PSY.json
python3 sankey.py "Secondary Education" SEED.json
python3 sankey.py "Sociology" SOC.json
python3 sankey.py "Special Education" SPE.json
python3 sankey.py "Wildlife and Fisheries Science" WFS.json
