import csv
import sys
import numpy as np

# configure numpy
np.set_printoptions(threshold=sys.maxsize)

# initialize major data structures
change_of_majors = dict()
majors = set()
with open('ChangeOfMajor2017.csv', newline='\n', encoding='utf-8-sig') as csvfile:
    com_reader = csv.reader(csvfile, delimiter=',')
    for row in com_reader:
        first_index = 0
        first_major_year = row[0].split()
# Start the analysis from Summer or Fall 2017
        while ((first_major_year[0] != '201750') and (first_major_year[0] != '201780')):
            first_index += 1
            first_major_year = row[first_index].split()

        most_recent_major = (row[-1].split())[-1]
        first_major = first_major_year[-1]

# For completeness and uniqueness
        majors.add(first_major)
        majors.add(most_recent_major)

# Ordered pair (from, to)
        edge = (first_major, most_recent_major)

# Add a new edge to the graph, or increase the magnitude
        if edge not in change_of_majors.keys():
            change_of_majors[edge] = 1
        else:
            value = change_of_majors[edge] + 1
            change_of_majors[edge] = value                

# Sort the set of majors to ease creation of the adjacency matrix
sorted_majors = sorted(majors)
num_majors = len(majors)

print(sorted_majors)

# Iterate through the dictionary, adding the edge counts to the matrix
adj_matrix = np.zeros( (num_majors, num_majors))
for key, value in change_of_majors.items():
    from_major = sorted_majors.index(key[0])
    to_major = sorted_majors.index(key[1])
    adj_matrix[from_major, to_major] = value

for i in range(num_majors):
    print ("[", end = ''),
    for j in range(num_majors-1):
        if (i == j):
            print("1", end=", "),
        else:
            print("{:0.0f}".format(adj_matrix[i,j]), end=", " ),
    print("1", end = ''),
    print ("],")

for i in range(num_majors):
    for j in range(num_majors):
        if (i != j):
            if (adj_matrix[i,j] != 0):
                print (sorted_majors[i], ",", sorted_majors[j], ",com")

print('\"nodes\":[')
for i in range(num_majors):
    print('{\"id\": \"', sorted_majors[i], '\"},')
#    print('{\"id\": \"', sorted_majors[i], '\", \"group\": 1},')
print('],')

print('\"links\":[')
for i in range(num_majors):
    for j in range(num_majors):
        if (i != j):
            if (adj_matrix[i,j] != 0):
                print ('{\"source\": \"', sorted_majors[i], '\" , \"target\": \"', sorted_majors[j], '\", \"value\": ', adj_matrix[i,j], '},')

print(']')