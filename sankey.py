import csv
import sys
import numpy as np

# configure numpy
np.set_printoptions(threshold=sys.maxsize)

# initialize major data structures
change_of_majors = dict()
majors = set()
with open('2017Cohortcsv.csv', newline='\n', encoding='utf-8-sig') as csvfile:
    com_reader = csv.reader(csvfile, delimiter=',')
    for row in com_reader:
        if row[0] == "F17":
            c0 = row[0]
            c1 = row[2]
            c2 = row[4]
            c3 = row[6]
            c4 = row[8]
        else:
            if row[0].strip().lower() == sys.argv[1].lower():
                n0 = row[0] + c0
                dc = row[1]
                n1 = row[2] + c1
                w0 = row[3]
                n2 = row[4] + c2
                w1 = row[5]
                n3 = row[6] + c3
                w2 = row[7]
                n4 = row[8] + c4
                w3 = row[9]
                majors.add(n0)
                majors.add(n1)
                majors.add(n2)
                majors.add(n3)
                majors.add(n4)
                if w0:
                    edge = (n0,n1)
                    if edge not in change_of_majors.keys():
                        change_of_majors[edge] = int(w0)
                    else:
                        value = change_of_majors[edge] + int(w0)
                        change_of_majors[edge] = value                    
                if w1:
                    edge = (n1,n2)
                    if edge not in change_of_majors.keys():
                        change_of_majors[edge] = int(w1)
                    else:
                        value = change_of_majors[edge] + int(w1)
                        change_of_majors[edge] = value
                if w2:
                    edge = (n2,n3)
                    if edge not in change_of_majors.keys():
                        change_of_majors[edge] = int(w2)
                    else:
                        value = change_of_majors[edge] + int(w2)
                        change_of_majors[edge] = value   
                if w3:
                    edge = (n3,n4)
                    if edge not in change_of_majors.keys():
                        change_of_majors[edge] = int(w3)
                    else:
                        value = change_of_majors[edge] + int(w3)
                        change_of_majors[edge] = value

sorted_majors = sorted(majors)
num_majors = len(majors)

adj_matrix = np.zeros( (num_majors, num_majors))
for key, value in change_of_majors.items():
    from_major = sorted_majors.index(key[0])
    to_major = sorted_majors.index(key[1])
    adj_matrix[from_major, to_major] = value

ofile = open(sys.argv[2], "w")

print('{\"nodes\":[', file = ofile)
for i in range(num_majors - 1):
   print('{\"name\": \"', sorted_majors[i], '\"},', file = ofile)
print('{\"name\": \"', sorted_majors[num_majors - 1], '\"}', file = ofile)
print('],', file = ofile)

links = []
print('\"links\":[', file = ofile)
for i in range(num_majors):
    for j in range(num_majors):
        if (i != j):
            if (adj_matrix[i,j] != 0):
                links.append('{\"source\": ' + str(i) + ', \"target\": ' + str(j) + ', \"value\": ' + str(adj_matrix[i,j]) + '}')
print(',\n'.join(links), file = ofile)

print(']}', file = ofile)
ofile.close()